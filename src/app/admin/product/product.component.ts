import { ProductDetailComponent } from './../product-detail/product-detail.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ApiService } from 'src/app/services/api.service';
import { FileUploaderComponent } from '../file-uploader/file-uploader.component';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
   title:any;
     //mendefinisikan variabel book sebagai objek
   book:any={};
   books:any=[];
  constructor(
    public dialog : MatDialog,
    public api: ApiService
  ) {
     this.title='Produk';
      //3. Memanggil fungsi getBooks()
     this.getBooks();
   }

  ngOnInit(): void {
  }
  //2. Membuat fungsi
  loading!: boolean;
  getBooks()
  {
    this.loading = true;
    this.api.get('bookswithauth').subscribe(result => {
      this.books=result;
      this.loading=false;
    }, err => {
      this.loading=false;
      alert('Ada masalah saat pengambilan data. Coba lagi!');
    })
  }

  productDetail(data: any,idx: number)
  {
   let dialog = this.dialog.open(ProductDetailComponent, {
      width: '400px',
      data:data
    });
    dialog.afterClosed().subscribe(res=>{
      if(res)
      {
        //jika idx=-1 (penambahan data baru) maka tambahkan data
        if(idx==-1)this.books.push(res);
        //jika tidak maka perbarui data
        else this.books[idx]=data;
      }
    })
 }

 loadingDelete: any={};
  deleteProduct(book: any, idx: any)
  {
    var conf = confirm('Delete item?');
    if(conf)
    {
      this.loadingDelete[idx]=true;
      this.api.delete('/bookswithauth///' + this.books[idx].id).subscribe(result=>{
      this.books.splice(idx,1);
      this.loadingDelete[idx]=false;
    },error=>{
      alert('Tidak dapat menghapus data, coba lagi');
      this.loadingDelete[idx]=false;
    });
    }
 }

  upload(data: any)
  {
  let dialog=this.dialog.open(FileUploaderComponent, {
  height: '300px',
  width: '400px',
  data:data
  });
  dialog.afterClosed().subscribe(res=>{
  return;
  })
}
  download(data: any)
  {
    FileSaver.saveAs('http://api.sunhouse.co.id/bookstore/' + data.url);
  }

}

